package com.example.homepage;

import java.io.Serializable;

class Homepage implements Serializable {

  private String homepageLocation;
  private static final long serialVersionUID = 5L;

  public Homepage(String location) {
    setHomepageLocation(location);
  }

  public void setHomepageLocation(String homepageLocation) {
    this.homepageLocation = homepageLocation;
  }

  public String getHomepageLocation() {
    return homepageLocation;
  }

}
