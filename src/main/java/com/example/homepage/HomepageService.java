package com.example.homepage;

import com.example.homepage.Homepage;
import com.example.io.ObjectReader;
import com.example.io.ObjectWriter;

public class HomepageService {

  private Homepage homepage;
  private final String homepageFilename = "homepage";
  private ObjectReader<Homepage> hReader = new ObjectReader<Homepage>(homepageFilename, new Homepage("http://example.com"));
  private ObjectWriter<Homepage> hWriter = new ObjectWriter<Homepage>(homepageFilename);

  public HomepageService() {
    homepage = readHomepage();
  }

  public String getHomepageLocation() {
    return homepage.getHomepageLocation();
  }

  public void setHomepage(String location) {
    Homepage newHomepage = new Homepage(location);
    this.homepage = newHomepage;
    writeHomepage(newHomepage);
  }

  private void writeHomepage(Homepage homepage) {
    hWriter.write(homepage);
  }

  private Homepage readHomepage() {
    Homepage homepage = hReader.read();
    return homepage;
  }
}
