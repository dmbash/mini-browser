package com.example.ui.controllers;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.stage.Stage;

import javafx.event.ActionEvent;

import javafx.fxml.FXML;

import javafx.scene.control.Button;
import javafx.scene.control.Label;

import com.example.BrowserWebEngine;
import com.example.util.Observer;

public class TitlebarController {

  private Stage stage;

  @FXML
  private Label title;

  @FXML
  private Button closeBtn;

  @FXML
  private void onCloseBtnClick(ActionEvent evt) {
    Platform.exit();
  }

  @FXML
  private Button minimizeBtn;

  @FXML
  private void onMinimizeBtnClick(ActionEvent evt) {
    stage.setIconified(true);
  }

  @FXML
  private Button maximizeBtn;

  @FXML
  private void onMaximizeBtnClick(ActionEvent evt) {
    stage.setMaximized(true);
  }

  @FXML
  public void initialize() {
    Platform.runLater(() -> {
      stage = (Stage) closeBtn.getScene().getWindow();
    });
    BrowserWebEngine.addLocationObserver(new Observer<String>() {
      @Override
      public void onUpdate(String newLocation) {
        title.setText(newLocation);
      }
    });
  }

}
