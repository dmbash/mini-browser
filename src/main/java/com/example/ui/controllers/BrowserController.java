package com.example.ui.controllers;


import javafx.event.ActionEvent;

import javafx.fxml.FXML;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;

import javafx.scene.web.WebView;

import com.example.BrowserWebEngine;
import com.example.util.SearchDirection;

public class BrowserController {

  @FXML
  private WebView webView;

  @FXML
  private HBox searchBox;

  @FXML
  private TextField searchBoxInput;

  @FXML
  private Label searchBoxLbl;

  @FXML
  public void onWebViewKP(KeyEvent evt) {
    if ((evt.getCode() == KeyCode.F) && (evt.isControlDown())) {
      searchBox.setVisible(!searchBox.isVisible());
      searchBoxInput.requestFocus();
    }
  }

  @FXML
  public void onSearchBoxPrevBtnClick(ActionEvent evt) {
    search(SearchDirection.BACKWARD);
  }

  @FXML
  public void onSearchBoxNextBtnClick(ActionEvent evt) {
    search(SearchDirection.FORWARD);
  }

  @FXML
  public void onSearchBoxCloseBtnClick(ActionEvent evt) {
    searchBox.setVisible(false);
  }

  @FXML
  public void onSearchBoxInputKP(KeyEvent evt) {
    if (evt.getCode() == KeyCode.ENTER) {
      search(SearchDirection.FORWARD);
    }
  }

  private void search(SearchDirection sd) {
    String template;
    String jsCall;
    if (sd == SearchDirection.FORWARD) {
      template = "window.find('%s')";
    } else {
      template = "window.find('%s', false, true)";
    }
    jsCall = String.format(template, searchBoxInput.getText());
    BrowserWebEngine.executeScript(jsCall);
  }


  @FXML
  public void initialize() {
    searchBox.managedProperty().bind(searchBox.visibleProperty());

    BrowserWebEngine.attachEngine(webView.getEngine());
    BrowserWebEngine.goHomepage();
  }
}
