package com.example.ui.controllers;

import javafx.application.Platform;
import javafx.concurrent.Worker.State;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import javafx.event.ActionEvent;

import javafx.fxml.FXML;

import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;

import com.example.BrowserWebEngine;
import com.example.bookmark.BookmarkService;
import com.example.util.Observer;

public class ToolbarController {

  private BookmarkService bService = BrowserWebEngine.getBookmarkService();

  @FXML
  private TextField addressInput;

  @FXML
  private ProgressBar progressBar;

  @FXML
  private void onAddressInputKP(KeyEvent evt) {
    if (evt.getCode() == KeyCode.ENTER) {
      BrowserWebEngine.go(addressInput.getText());
    }
  }

  @FXML
  private void onHistoryBackBtnClick(ActionEvent evt) {
    BrowserWebEngine.historyBack();
  }

  @FXML
  private void onHistoryForwardBtnClick(ActionEvent evt) {
    BrowserWebEngine.historyForward();
  }

  @FXML
  private void onRefreshBtnClick(ActionEvent evt) {
    BrowserWebEngine.reload();
  }

  @FXML
  private void onHomeBtnClick(ActionEvent evt) {
    BrowserWebEngine.goHomepage();
  }

  @FXML
  private void onHomeSetBtnClick(ActionEvent evt) {
    BrowserWebEngine.setCurrentLocationAsHomepage();
  }

  @FXML
  private void onBookmarkBtnClick(ActionEvent evt) {
    bService.addBookmark(BrowserWebEngine.getCurrentURL());
  }

  @FXML
  public void initialize() {
    BrowserWebEngine.addLocationObserver(new Observer<String>() {
      @Override
      public void onUpdate(String newLocation) {
        addressInput.setText(newLocation);
      }
    });

    progressBar.managedProperty().bind(progressBar.visibleProperty());

    BrowserWebEngine.addLoadWorkerObserver(new Observer<State>() {
      @Override
      public void onUpdate(State newState) {
        if (newState == State.SUCCEEDED) {
          progressBar.setVisible(false);
        } else {
          progressBar.setVisible(true);
        }
      }
    });

    Platform.runLater(() -> {
      progressBar.progressProperty().bind(BrowserWebEngine.getLoadWorker().progressProperty());
    });

  }

}
