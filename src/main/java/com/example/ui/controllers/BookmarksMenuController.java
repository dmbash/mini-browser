package com.example.ui.controllers;

import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import javafx.geometry.Bounds;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;
import javafx.stage.Stage;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;

import com.example.BrowserWebEngine;
import com.example.bookmark.Bookmark;
import com.example.bookmark.BookmarkService;
import java.io.IOException;

public class BookmarksMenuController {

  private Popup bPopup;

  @FXML
  private Button bookmarksBtn;

  private BookmarkService bService = BrowserWebEngine.getBookmarkService();

  @FXML
  private void onBookmarksBtnClick(ActionEvent evt) {
    Stage stage = com.example.App.getStage();

    List<Bookmark> bookmarks = bService.getBookmarks();

    VBox bPopupVbox = (VBox) ((ScrollPane) bPopup.getContent().get(0)).getContent();
    bPopupVbox.getChildren().clear();

    for (Bookmark bookmark : bookmarks) {
      HBox bEntry = getBookmarkEntry(bookmark);
      Button bEntryBtn = (Button) bEntry.getChildren().get(1);
      bEntryBtn.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent evt) {
          bService.removeBookmark(bookmark);
          bPopupVbox.getChildren().remove(bEntry);
        }
      });
      bPopupVbox.getChildren().add(bEntry);
    }

    Bounds bounds = bookmarksBtn.localToScene(bookmarksBtn.getBoundsInLocal());
    bPopup.setX(bounds.getMinX() - bPopup.getWidth());
    bPopup.setY(bounds.getMaxY() + 40);

    bPopup.show(stage);
  }

  private Popup initBookmarkPopup() {
    FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/com/example/bookmark/bookmarks_popup.fxml"));
    Parent root;
    try {
      root = fxmlLoader.load();
    } catch (IOException ex) {
      ex.printStackTrace();
      return null;
    }

    Popup popup = new Popup();

    popup.setAutoHide(true);
    popup.getContent().add(root);
    return popup;
  }

  private HBox getBookmarkEntry(Bookmark bookmark) {
    FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/com/example/bookmark/bookmark.fxml"));
    HBox entry;
    try {
      entry = fxmlLoader.load();
      Label entryLbl = (Label) entry.getChildren().get(0);
      entryLbl.setText(bookmark.getBookmarkLocation());
      entryLbl.setOnMouseClicked(new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent evt) {
          BrowserWebEngine.go(bookmark.getBookmarkLocation());
        }
      });
    } catch (IOException ex) {
      ex.printStackTrace();
      return null;
    }

    return entry;
  }

  @FXML
  public void initialize() {
    bPopup = initBookmarkPopup();
  }
}
