package com.example;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class App extends Application {

    private static Scene scene;
    private static Stage stage;

    @Override
    public void start(Stage stage) throws IOException {
      setStage(stage);
      scene = new Scene(loadFXML("app"), 640, 480);
      stage.setScene(scene);
      stage.show();
    }

    private static Parent loadFXML(String fxml) throws IOException {
      FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
      return fxmlLoader.load();
    }

    public static void main(String[] args) {
      launch();
    }

    public static void setStage(Stage stage) {
      App.stage = stage;
    }

    public static Stage getStage() {
      return stage;
    }
}
