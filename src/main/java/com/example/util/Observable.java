package com.example.util;

import java.util.ArrayList;
import java.util.List;

import com.example.util.Observer;

public class Observable<T extends Observer<V>, V> {
  private List<T> observers = new ArrayList<T>();
  private V observableValue;

  public void addObserver(T observer) {
    this.observers.add(observer);
  }

  public void removeObserver(T observer) {
    this.observers.remove(observer);
  }

  public V getValue() {
    return observableValue;
  }

  public void setValue(V value) {
    this.observableValue = value;
    update();
  }

  public void update() {
    for (T observer: observers) {
      observer.onUpdate(observableValue);
    }
  }
}
