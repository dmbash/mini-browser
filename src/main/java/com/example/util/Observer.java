package com.example.util;

public interface Observer<V> {
  public void onUpdate(V observableValue);
}
