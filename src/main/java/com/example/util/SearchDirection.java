package com.example.util;

public enum SearchDirection {
  BACKWARD,
  FORWARD
}
