package com.example.bookmark;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.example.bookmark.Bookmark;
import com.example.io.ObjectReader;
import com.example.io.ObjectWriter;

public class BookmarkService {

  private final String bookmarksFilename = "bookmarks";
  private List<Bookmark> bookmarks = new ArrayList<Bookmark>();
  private ObjectReader<List<Bookmark>> bReader = new ObjectReader<List<Bookmark>>(bookmarksFilename, new ArrayList<Bookmark>());
  private ObjectWriter<List<Bookmark>> bWriter = new ObjectWriter<List<Bookmark>>(bookmarksFilename);

  public BookmarkService() {
    this.bookmarks = readBookmarks();
  }

  public List<Bookmark> getBookmarks() {
    return bookmarks;
  }

  public void addBookmark(Bookmark bookmark) {
    if (this.bookmarks.contains(bookmark)) {
      return;
    }

    this.bookmarks.add(bookmark);
    writeBookmarks();
  }

  public void addBookmark(String bLocation) {
    Boolean bookmarkExists = false;

    Iterator<Bookmark> iterator = this.bookmarks.iterator();
    while(iterator.hasNext()) {
      Bookmark bookmark = iterator.next();
      if (bookmark.getBookmarkLocation().equalsIgnoreCase(bLocation)) {
        bookmarkExists = true;
      }
    }

    if (bookmarkExists) {
      return;
    }

    this.bookmarks.add(new Bookmark(bLocation));
    writeBookmarks();
  }

  public void removeBookmark(Bookmark bookmark) {
    if (this.bookmarks.contains(bookmark)) {
      this.bookmarks.remove(bookmark);
      writeBookmarks();
    }
  }

  public void setBookmarks(List<Bookmark> bookmarks) {
    this.bookmarks = bookmarks;
    writeBookmarks();
  }

  private void writeBookmarks() {
    bWriter.write(this.bookmarks);
  }

  private List<Bookmark> readBookmarks() {
    List<Bookmark> readedBookmarks = bReader.read();
    return readedBookmarks;
  }
}
