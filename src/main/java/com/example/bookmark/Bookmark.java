package com.example.bookmark;

import java.io.Serializable;

public class Bookmark implements Serializable {
  private static final long serialVersionUID = 2L;
  private String bookmarkLocation;

  public Bookmark(String location) {
    setBookmarkLocation(location);
  }

  public void setBookmarkLocation(String bookmarkLocation) {
    this.bookmarkLocation = bookmarkLocation;
  }

  public String getBookmarkLocation() {
    return bookmarkLocation;
  }
}
