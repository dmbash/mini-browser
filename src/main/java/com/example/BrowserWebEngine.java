package com.example;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebHistory;

import com.example.homepage.HomepageService;
import com.example.bookmark.BookmarkService;
import com.example.util.Observable;
import com.example.util.Observer;

public class BrowserWebEngine {

  private static WebEngine webEngine;
  private static WebHistory webHistory;
  private static HomepageService homepageService = new HomepageService();
  private static BookmarkService bookmarkService = new BookmarkService();
  private static Observable<Observer<String>, String> locationObservable = new Observable<Observer<String>, String>();
  private static Observable<Observer<State>, State> loadWorkerStateObservable = new Observable<Observer<State>, State>();

  private BrowserWebEngine() {
  }

  public static void executeScript(String script) {
    webEngine.executeScript(script);
  }

  public static void attachEngine(WebEngine we) {
    webEngine = we;
    webHistory = we.getHistory();
    initHandlers();
  }

  public static void goHomepage() {
    go(homepageService.getHomepageLocation());
  }

  public static void setCurrentLocationAsHomepage() {
    homepageService.setHomepage(getCurrentURL());
  }

  private static void initHandlers() {
    setLocationHandler();
    setLoadWorkerHandler();
  }

  private static void setLocationHandler() {
    webEngine.locationProperty().addListener(new ChangeListener<String>() {
      @Override
      public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        locationObservable.setValue(newValue);
      }
    });
  }

  public static void addLocationObserver(Observer<String> locationObserver) {
    locationObservable.addObserver(locationObserver);
  }

  private static void setLoadWorkerHandler() {
    webEngine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
      @Override
      public void changed(ObservableValue<? extends State> observable, State oldState, State newState) {
        loadWorkerStateObservable.setValue(newState);
      }
    });
  }

  public static void addLoadWorkerObserver(Observer<State> loadWorkerObserver) {
    loadWorkerStateObservable.addObserver(loadWorkerObserver);
  }

  public static void historyBack() {
    if (webHistory.getCurrentIndex() != 0) {
      webHistory.go(-1);
    }
  }

  public static void historyForward() {
    if (webHistory.getCurrentIndex() != webHistory.getEntries().size()-1) {
      webHistory.go(1);
    }
  }

  public static void go(String url) {
    webEngine.load(url);
  }

  public static void reload() {
    webEngine.reload();
  }

  public static Worker<Void> getLoadWorker() {
    return webEngine.getLoadWorker();
  }

  public static String getCurrentURL() {
    return webEngine.getLocation();
  }

  public static BookmarkService getBookmarkService() {
    return bookmarkService;
  }
}
