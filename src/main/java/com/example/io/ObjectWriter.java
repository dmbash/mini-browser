package com.example.io;

import java.io.FileOutputStream;

import com.example.io.Writer;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ObjectWriter<T> implements Writer<T> {
  private String filename;

  public ObjectWriter(String filename) {
    this.filename = filename;
  }

  public void write(T object) {
    try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))) {
      out.writeObject(object);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
