package com.example.io;

public interface Reader<T> {
  public T read();
}
