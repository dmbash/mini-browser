package com.example.io;

public interface Writer<T> {
  public void write(T o);
}
