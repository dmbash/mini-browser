package com.example.io;

import java.io.FileInputStream;
import java.lang.ClassNotFoundException;

import com.example.io.Reader;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ObjectReader<T> implements Reader<T> {

  private T realObject;
  private T fallbackObject;
  private String filename;

  public ObjectReader(String filename, T fallbackObject) {
    this.filename = filename;
    this.fallbackObject = fallbackObject;
  }

  @SuppressWarnings("unchecked")
  public T read() {
    try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))) {
      realObject = (T) in.readObject();
    } catch (IOException | ClassNotFoundException ex) {
      return fallbackObject;
    }
    return realObject;
  }
}
